const express = require('express')
const hbs = require('hbs')
const fs = require('fs')

const port = process.env.PORT || 3000
var app = express()

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs')


app.use((req, res, next) => {
  var now = new Date().toString();
  const log = `${now}: ${req.method} ${req.url}}`
  console.log(log)
  fs.appendFile('server.log', log + '\n', (err) => {
    if(err)console.log(err)
  })
  next()
})

/*app.use((req, res, next) => {
  res.render('maintenance.hbs')
})*/

app.use(express.static(__dirname + '/public'))

hbs.registerHelper('getCurrentYear', () => {
  return new Date().getFullYear()
})

hbs.registerHelper('screamIt', (text) => {
  return text.toUpperCase();
})

app.get('/', (req, res) => {
  res.render('home.hbs',{
    pageTitle: 'Home Page',
    welcomeMessage: 'Content of Home Page',
  })
})

app.get('/about', (req, res) => {
  res.render('about.hbs',{
    pageTitle: 'About Page',
  })
})

app.get('/portfolio', (req, res) => {
  res.render('portfolio.hbs',{
    pageTitle: 'Portfolio Page',
    content: 'Here is the <a href="https://gitlab.com/razorf80/node-web-server" target="_blank">link</a> to the project',
  })
})

app.get('/bad', (req, res) => {
  res.send({
    errorMessage: 'Unabled to load page'
  })
})

app.listen(port, () => {
  console.log(`Server succesfully startes on port ${port}`)
})